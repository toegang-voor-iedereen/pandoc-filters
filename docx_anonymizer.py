import argparse
import os
import lxml.etree
import random


def is_file(value: str) -> str:
    if os.path.isfile(value):
        return value

    raise argparse.ArgumentTypeError(f"'{value}' is not a file.")


def replace_with_random(char):
    if char.islower():
        return random.choice('abcdefghijklmnopqrstuvwxyz')
    elif char.isupper():
        return random.choice('ABCDEFGHIJKLMNOPQRSTUVWXYZ')
    elif char.isdigit():
        return random.choice('0123456789')
    return char


def process_text(element):
    # Using a list comprehension to apply the replace function to each character in the text
    element.text = ''.join([replace_with_random(char) for char in element.text])


def main():
    parser = argparse.ArgumentParser(description="Anonimize a `.docx` file. " +
                                                 "To use it, please first extract the `.docx` to a directory: " +
                                                 "`$ unzip test/example/input.docx -d test/example/_docx`. " +
                                                 "Then call this application on that directory ('test/example/_docx').")

    parser.add_argument('path_to_extracted_docx', type=is_file, help='Path to the extracted docx file.')

    args = parser.parse_args()

    with open(args.path_to_extracted_docx, 'rb') as f:
        xml_content = f.read()

    root = lxml.etree.fromstring(xml_content)

    text_elements = root.findall('.//w:t', namespaces=root.nsmap)

    for element in text_elements:
        process_text(element)

    with open(args.path_to_extracted_docx, 'w') as f:
        f.write(lxml.etree.tostring(root,
                                    pretty_print=True,
                                    encoding='unicode'))


if __name__ == "__main__":
    main()
