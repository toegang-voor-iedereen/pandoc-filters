---
date: "2023-05-04"
title: "Pandoc Filters"
description: "README-file of pandoc-filters"
author: "Stephan Meijer"
license: "EUPL-1.2"
version: "0.0.1"
homepage: "https://www.toegangvooriedereen.nl"
---

# Pandoc Filters

## Goal

This repository contains a collection of filters for [Pandoc](http://pandoc.org/). The goal is to provide a set of filters for cleaning some things.

## Usage

```bash
$ pandoc --from=docx --to=html -s            \
         --template=src/template.html        \
         --lua-filter=src/filters/Header.lua \
         --lua-filter=src/filters/Image.lua  \
         --lua-filter=src/filters/List.lua   \
         --lua-filter=src/filters/Para.lua   \
         --lua-filter=src/filters/Span.lua   \
         tests/happy-flow/input.docx
```
## Tests

This repository contains some tests. These tests are located in the `tests` directory.

These tests are based on snapshot / blackbox-principles. Output of these tests can differ from the expected output. This is because the output of Pandoc can differ from version to version.

```bash
$ python3 ./test/run_tests.py
```

### Installation of Pandoc

Running tests might require to install a later version of Pandoc than what has last been released, as we sometimes make patches to Pandoc.

#### Mac

On Mac, you could run:

    $ brew install --build-from-source --HEAD pandoc

You could also save [the Formula](https://github.com/Homebrew/homebrew-core/blob/master/Formula/pandoc.rb) to a local file (such as `pandoc.rb`), modify it and run:

    $ brew install ./pandoc.rb

#### Ubuntu or Debian

On Ubuntu- or Debian-based systems, you can run `.ci/install-pandoc.sh`.

#### Others

Most Linux distro's have ways to build newer commits from source. Please reference [the Pandoc documentation](https://pandoc.org/installing.html) or the documentation of your package manager of choice.

When using Windows, I wish you luck.

## Template

Template is based on the standalone template from Pandoc, but it strips out styling and add a lot of meta tags.

<details>
<summary>Description of <code>&lt;meta&gt;</code> tags</summary>

<table>
    <thead>
        <tr>
            <th>Tag</th>
            <th>Description</th>
            <th>Relevance to NLdoc</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td><code>generator</code></td>
            <td>
                Value is always <code>nldoc:pandoc</code>. Indicates that the
                document has been converted using this specific template.
            </td>
            <td>Could be used in the Element Structure Converter.</td>
        </tr>
        <tr>
            <td><code>author</code></td>
            <td>Includes all known authors on document.</td>
            <td>Potentially relevant in the far future.</td>
        </tr>
        <tr>
            <td><code>dcterms.date</code></td>
            <td>
                A date relating to the document. In most cases probably a
                date of publication.
            </td>
            <td>Potentially relevant in the far future.</td>
        </tr>
        <tr>
            <td><code>keywords</code> and <code>nldoc:keyword</code></td>
            <td>Keywords relating to the document</td>
            <td>Potentially relevant in the far future.</td>
        </tr>
        <tr>
            <td><code>description</code></td>
            <td>Description of document</td>
            <td>Potentially relevant in the far future.</td>
        </tr>
        <tr>
            <td><code>nldoc:title-prefix</code></td>
            <td>A title prefix of a document.</td>
            <td>Potentially relevant.</td>
        </tr>
        <tr>
            <td><code>nldoc:title</code></td>
            <td>Title of a document</td>
            <td>
                Highly relevant as this allows us to easily determine
                the title of a document.
            </td>
        </tr>
        <tr>
            <td><code>nldoc:subtitle</code></td>
            <td>Subtitle of a document</td>
            <td>
                Highly relevant as this allows us to easily determine
                the subtitle of a document.
            </td>
        </tr>
        <tr>
            <td><code>nldoc:abstract-title</code></td>
            <td>Title of abstract of document.</td>
            <td>
                Probably irrelevant as chances are low users will actually
                use this in their documents.
            </td>
        </tr>
        <tr>
            <td><code>nldoc:abstract</code></td>
            <td>Abstract of document.</td>
            <td>
                Probably irrelevant as chances are low users will actually
                use this in their documents.
            </td>
        </tr>
        <tr>
            <td><code>nldoc:subject</code></td>
            <td>Subject of document.</td>
            <td>
                Probably irrelevant as chances are low users will actually
                use this in their documents.
            </td>
        </tr>
        <tr>
            <td><code>nldoc:category</code></td>
            <td>Category of document.</td>
            <td>
                Probably irrelevant as chances are low users will actually
                use this in their documents.
            </td>
        </tr>
        <tr>
            <td><code>nldoc:direction</code></td>
            <td>Text direction in document.</td>
            <td>
                <p>
                    Probably irrelevant as chances are low users will actually
                    use this in their documents.
                </p>
                <p>
                    Maybe ever relevant if we are going international.
                </p>
            </td>
        </tr>
        <tr>
            <td><code>nldoc:lang</code></td>
            <td>Language of document.</td>
            <td>
                <p>
                    Probably irrelevant as chances are low users will actually
                    use this in their documents.
                </p>
                <p>
                    Maybe ever relevant if we are going international.
                </p>
            </td>
        </tr>
    </tbody>
</table>

</details>

## Scripts

### `src/filters/List.lua`
`<li>` tags containing `<blockquote>` / `BlockQuote` as first element will be transformed and replaced so it will contain the elements of the `BlockQuote` instead of the `BlockQuote` itself, followed by other elements if applicable

<details>
<summary>Before</summary>

```html
<ul>
    <li>
        <blockquote>
            <p>This is a list.</p>
        </blockquote>
    </li>
    <li>
        <blockquote>
            <p>It uses ‘-’ in the source document.</p>
        </blockquote>
    </li>
    <li>
        <blockquote>
            <p>It is unordered.</p>
        </blockquote>
        <ul>
            <li>
                <blockquote>
                    <p>This is a sub item on a list.</p>
                </blockquote>
                <ul>
                    <li>
                        <blockquote>
                            <p>This is a sub-sub-item.</p>
                        </blockquote>
                    </li>
                </ul>
            </li>
            <li>
                <blockquote>
                    <p>Another sub-item.</p>
                </blockquote>
            </li>
        </ul>
    </li>
    <li>
        <blockquote>
            <p>And back to the original level.</p>
        </blockquote>
    </li>
</ul>
```
</details>
<details>
    <summary>After</summary>

```html
<ul>
  <li>
    <p>This is a list.</p>
  </li>
  <li>
    <p>It uses ‘-’ in the source document.</p>
  </li>
  <li>
    <p>It is unordered.</p>
    <ul>
      <li>
        <p>This is a sub item on a list.</p>
        <ul>
          <li>
            <p>This is a sub-sub-item.</p>
          </li>
        </ul>
      </li>
      <li>
        <p>Another sub-item.</p>
      </li>
    </ul>
  </li>
  <li>
    <p>And back to the original level.</p>
  </li>
</ul>
```

</details>

### `src/filters/Header.lua`

Headers containing nothing or `LineBreak` / `<br />` will be removed.

<details>
<summary>Before</summary>

```html
<h1 id="section-1"><br /></h1>
<h1 id="how-was-the-previous-alt-text-added">How was the previous alt-text added?</h1>
<p>See the next image.</p>
```
</details>

<details>
<summary>After</summary>

```html
<h1 id="how-was-the-previous-alt-text-added">How was the previous alt-text added?</h1>
<p>See the next image.</p>
```
</details>

### `src/filters/Image.lua`

Attributes such as `id` and `class`, `style`, `width`, `height` are being removed from `<img>` / `Image`.

<details>
<summary>Before</summary>

```html
<p><img src="media/image1.png" style="width:6.26772in;height:4.18056in" alt="This is an alt description." /></p>
<p><img src="media/image2.png" style="width:6.26772in;height:4.22222in" alt="Alt Description." /></p>
```
</details>

<details>
<summary>After</summary>

```html
<p><img src="media/image1.png" alt="This is an alt description." /></p>
<p><img src="media/image2.png" alt="Alt Description." /></p>
```
</details>

### `src/filters/Span.lua`

Anchors will be removed.

<details>
<summary>Before</summary>

```html
<header id="title-block-header">
    <h1 class="title">
        <span id="_sc0do1nhsdpl" class="anchor"></span>
        Hoofdtitel
    </h1>
</header>
<p>This document has been made with Google Docs.</p>
```
</details>

<details>
<summary>After</summary>

```html
<header id="title-block-header">
    <h1 class="title">
        Hoofdtitel
    </h1>
</header>
<p>This document has been made with Google Docs.</p>
```
</details>

### `src/filters/Para.lua`

Paragraphs containing only one element will be replaced by that element.

<details>
<summary>Before</summary>

```html
<p><img src="media/image1.png" alt="This is an alt description." /></p>
<p><img src="media/image2.png" alt="Alt Description." /></p>
```
</details>

<details>
<summary>After</summary>

```html
<img src="media/image1.png" alt="This is an alt description." />
<img src="media/image2.png" alt="Alt Description." />
```
</details>
