#!/bin/bash

set -eu

echo "# Release \`$CI_COMMIT_TAG\`"
echo ""
echo "## Pandoc version info (\`$ pandoc -v\`)"
echo ""
echo '```'
pandoc -v
echo '```'
echo ""

cat CHANGELOG.md
