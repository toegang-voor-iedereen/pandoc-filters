#!/bin/bash

PANDOC_VERSION="3.1.9"

function echo_var {
    echo -ne "\033[1;35m$1\033[0m"
    echo -ne "\033[1m=\033[0m"
    echo -ne "\033[1;36m$2\033[0m"
    echo ""
}

set -eu

OS="$(uname -s)"
ARCH="$(uname -m)"

DISTRO="unknown"

if [[ -f /etc/os-release ]]; then
    DISTRO="$(cat /etc/os-release | grep "^ID=" | cut -d '=' -f 2)"
fi

if [ "$DISTRO" = "ubuntu" ]; then
    DISTRO="debian"
fi

if [[ "$ARCH" = "aarch64" ]]; then
    ARCH="arm64"
elif [[ "$ARCH" = "x86_64" ]]; then
    ARCH="amd64"
fi


if command -v asdf >/dev/null 2>&1; then
    asdf plugin-add pandoc
    asdf install pandoc "$PANDOC_VERSION"
    asdf global pandoc "$PANDOC_VERSION"
elif [[ "$OS" = "Linux" && ( "$ARCH" = "amd64" || "$ARCH" = "arm64" ) ]]; then
    if [[ "$OS" = "Linux" && "$DISTRO" = "debian" ]]; then
        apt-get update
        apt-get -y --no-install-recommends install wget ca-certificates tar
    elif [[ "$OS" = "Linux" && "$DISTRO" = "alpine" ]]; then
        apk add --no-cache wget tar
    fi

    PANDOC_TAR_URL="https://github.com/jgm/pandoc/releases/download/$PANDOC_VERSION/pandoc-$PANDOC_VERSION-linux-$ARCH.tar.gz"
    PANDOC_TAR_PATH="/tmp/pandoc.tar.gz"
    PANDOC_PATH="/tmp/pandoc"

    rm -rf "$PANDOC_PATH"

    wget -q "$PANDOC_TAR_URL" -O "$PANDOC_TAR_PATH"
    mkdir "$PANDOC_PATH"
    tar xvzf "$PANDOC_TAR_PATH" -C "$PANDOC_PATH"
    rm "$PANDOC_TAR_PATH"

    mv /tmp/pandoc/*/bin/* /usr/local/bin/

    rm -rf "$PANDOC_PATH"
else
    echo -e "\033[1;31mUnsupported combination of operating system, distro and architecture:\033[0m" >&2
    echo_var "OS" "$OS" >&2
    echo_var "DISTRO" "$DISTRO" >&2
    echo_var "ARCH" "$ARCH" >&2
    exit 1
fi

pandoc -v
