# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [0.0.12] - 2023-08-10

- Various test cases

## [0.0.11] - 2023-08-03

- NLDOC-777 / NLDOC-738: Image in Paragraphs/Headings are now splitted to separate elements outside them.

## [0.0.8] - 2023-07-04

### Fixed

- No longer `<strong>`, `<em>` or `<mark>` tags inside Headers.
- Strip identifier / `id="...""` from Headers.

## [0.0.7] - 2023-07-03

### Fixed

- NLDOC-676 / NLDOC-679: Anchors from `.odt` are now removed.
- NLDOC-676 / NLDOC-677 On `Para`, first and only child is only returned if it is an Image.

## [0.0.6] - 2023-06-28

### Added

- [pre-commit](https://pre-commit.com/), see `.pre-commit-config.yaml`.
- `.editorconfig`

## [0.0.5] - 2023-06-28

### Changed

- Add `CHANGELOG.md` and add it to all Gitlab Releases after tagging

### Fixed

- CI / CD fix

## [0.0.4] - 2023-06-28

### Changed

- Support for test cases that use `.odt` or `.epub`

### Added

- Test cases `google-docs-title-subtitle-headings-image-lists-odt` and `google-docs-title-subtitle-headings-image-lists-docx`
