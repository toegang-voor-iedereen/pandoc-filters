#!/bin/env python3

import unittest
import sys
import os
import subprocess
import difflib
import argparse
import typing

from typing import List, Optional

class InputFormat:
    pandoc_type: str
    extension: str

    def __init__(self, pandoc_type: str, extension: typing.Optional[str] = None):
        if extension is None:
            extension = pandoc_type

        self.pandoc_type = pandoc_type
        self.extension = extension

    def input_filename(self) -> str:
        return f"input.{self.extension}"


def get_path(path: Optional[str] = None) -> str:
    current_file_dir = os.path.dirname(os.path.abspath(__file__))

    if path is None:
        return current_file_dir

    return os.path.join(current_file_dir, path)

class Tests(unittest.TestCase):
    inputFormats: typing.List[InputFormat] = [InputFormat("docx"), InputFormat("odt"), InputFormat("epub")]

    def assert_snapshot(self, snapshot: str, name: str, filters: List[str]):
        filters = [f"--lua-filter={filter}" for filter in filters]

        src_template_path = get_path("./src/template.html")

        snapshot_dir = get_path(os.path.join('test', snapshot))
        output_dir = get_path(os.path.join(snapshot_dir, name))

        if not os.path.exists(output_dir):
            os.makedirs(output_dir)

        actual_file = os.path.join(output_dir, "actual.html")
        expected_file = os.path.join(output_dir, "expected.html")

        format = next(( format for format in self.inputFormats
                        if os.path.isfile(os.path.join(snapshot_dir, format.input_filename()))), None)

        if format is None:
            return

        cmd = ["pandoc", "-s", "--quiet", f"--from={format.pandoc_type}", "--to=html", f"--output={actual_file}",
               f"--template={src_template_path}", "--track-changes=all", *filters, os.path.join(snapshot_dir, format.input_filename())]

        subprocess.run(cmd, check=True, shell=False)

        if not os.path.exists(expected_file):
            with open(expected_file, "w") as f_expected:
                with open(actual_file, 'r') as f_actual:
                    f_expected.write(f_actual.read())

        with open(actual_file, 'r') as f:
            actual_content = f.read()

        with open(expected_file, 'r') as f:
            expected_content = f.read()

        if actual_content != expected_content:
            diff = "".join(difflib.unified_diff([f"{line}\n" for line in expected_content.split("\n")],
                                                [f"{line}\n" for line in actual_content.split("\n")],
                                                fromfile=expected_file,
                                                tofile=actual_file))
            self.fail(f"Snapshot {snapshot} failed:\n\n{diff}")


def get_all_snapshots() -> List[str]:
    test_dir = get_path('./test')
    return [snapshot for snapshot in os.listdir(test_dir) if os.path.isdir(test_dir) and not snapshot.startswith('.DS_Store')]


def create_test_fn(snapshot: str, name: str, filters: List[str]):
    def test_fn(self):
        self.assert_snapshot(snapshot, name, filters)

    return test_fn


def create_tests_for_snapshot(snapshot: str):
    src_filters_dir = get_path("./src/filters")

    filters = [(os.path.splitext(filter_filename)[0],
                os.path.join(src_filters_dir, filter_filename)) for filter_filename in os.listdir(src_filters_dir)]

    fn_name_prefix = f"test_{snapshot.replace('-', '_')}_filter"

    for filter_name, absolute_path in filters:
        setattr(Tests,
                f"{fn_name_prefix}_{filter_name}",
                create_test_fn(snapshot, filter_name, [absolute_path]))

    setattr(Tests,
            f"{fn_name_prefix}_all",
            create_test_fn(snapshot, "all", [absolute_path for _, absolute_path in filters]))

    setattr(Tests, f"{fn_name_prefix}_none", create_test_fn(snapshot, "none", []))


if __name__ == '__main__':
    for snapshot in get_all_snapshots():
        create_tests_for_snapshot(snapshot)

    unittest.main(verbosity=3)
