-- Strip `<em>`, `<strong>`, `<mark>` from Header.
function stripMarkupFromHeaderContent(elem)
    elem.content = pandoc.walk_inline(pandoc.Span(elem.content), {
        Strong = function(el) return el.content end,
        Emph = function(el) return el.content end,
        Mark = function(el) return el.content end,
    }).content

    return elem
end

function Header(elem)
    local has_only_br = #elem.content == 1 and elem.content[1].t == "LineBreak"
    local has_no_content = #elem.content == 0

    -- If Header has no content, then throw it.
    if has_only_br or has_no_content then
        return {}
    end

    -- Strip `<em>`, `<strong>`, `<mark>` from Header.
    elem = stripMarkupFromHeaderContent(elem)

    -- Strip identifier from Header
    elem.identifier = ""

    return elem
end

return {
    {Header = Header}
}
