-- Fix, propbably for Google Docs.
function process_list_items(list)
    for i, items_in_list in ipairs(list) do
        if #items_in_list > 0 and items_in_list[1].tag == "BlockQuote" then
            if type(items_in_list[1].c) == "table" then
                new_table = items_in_list[1].c

                for j = 2, #items_in_list do
                    table.insert(new_table, items_in_list[j])
                end

                list[i] = new_table
            end
        end
    end
    return list
end

function process_lists(list_type)
    return function(elem)
        elem.content = process_list_items(elem.content)
        return elem
    end
end

return {
    {OrderedList = process_lists('OrderedList')},
    {BulletList = process_lists('BulletList')}
}
