function elementContainsClass(element, class_name)
    for _, class in ipairs(element.classes) do
        if class == class_name then
            return true
        end
    end

    return false
end

function elementHasId(element, id_name)
    return element.identifier == id_name
end

function elementHasIdStartingWith(element, id_prefix)
    return string.sub(element.identifier, 1, string.len(id_prefix)) == id_prefix
end

function isAnchorSpan(span)
    return elementContainsClass(span, "anchor") or
        elementHasId(span, "anchor") or
        elementHasIdStartingWith(span, "anchor-")
end

function Span(span)
    if isAnchorSpan(span) then
        return {}
    end

    return span
end
