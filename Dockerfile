FROM python:latest

RUN mkdir /pandoc-filters

WORKDIR /pandoc-filters

COPY . .

RUN apt update && \
    apt-get -y --no-install-recommends install wget ca-certificates tar

RUN python3 -mpip install -r requirements.txt

RUN bash .ci/install-pandoc.sh

ENTRYPOINT [ "python3", "run_tests.py" ]
