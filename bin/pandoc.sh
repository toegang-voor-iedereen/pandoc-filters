#!/bin/bash
# This script enriches pandoc calls with the template-file and filters from this
# repository so you don't have to manually change applications using it when doing
# conversions.

set -eu

CURRENT_DIR="$(realpath "$(dirname "$0")")"
FILTERS_DIR="$CURRENT_DIR/../src/filters"

TEMPLATE_FILE="$CURRENT_DIR/../src/template.html"

# Initialize an empty string to hold the lua filters
LUA_FILTERS=()

# Use a for loop to iterate over each .lua file
for FILTER in "$FILTERS_DIR"/*.lua; do
    LUA_FILTERS+=("--lua-filter=$FILTER")
done

# --track-changes=all was disabled

pandoc -s --quiet --to=html  --template="$TEMPLATE_FILE"  "${LUA_FILTERS[@]}" "$@"
